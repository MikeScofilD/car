from car.Car import Car


def main():
    escalade = Car('Cadillac', 100, 10)
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.fill_tank()
    escalade.fill_tank()
    escalade.fill_tank()
    escalade.fill_tank()
    escalade.fill_tank()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.drive()
    escalade.fill_tank()
    print(escalade)


if __name__ == '__main__':
    main()
