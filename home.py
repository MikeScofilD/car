class Flat:
    def __init__(self, number, is_open):
        self.number = number
        self.is_open = is_open

    def open_dore(self):
        self.is_open = True

    def close_dore(self):
        self.is_open = False


class Mouse:
    def __init__(self, arr=None):
        arr.append(self.Flat(14, True))

    def check_open(self):
        pass

    def find_flat(self):
        raise Exception('Flat Not Found Error')
