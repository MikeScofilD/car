class Car:
    def __init__(self, model, fuel, rate_fuel):
        """Constructor"""
        self.model = model  # Название авто
        self.fuel = fuel  # Топливо в баке
        self.rate_fuel = rate_fuel  # Расход топлива

    def drive(self):
        """
        Drive the car
        """
        if self.fuel < self.rate_fuel:
            #
            print(f"В автомобиле закончлися запас топлива заполните бак")
        else:
            self.fuel = self.fuel - self.rate_fuel
            print(f"В автомобиле {self.model} осталось {self.fuel} ед. топлива")

    def brake(self):
        """
        Stop the car
        """
        return "Braking"

    def fill_tank(self):
        """
            Fill tank
        """
        if self.fuel < 100:
            self.fuel = self.fuel + 25
            print(f"Вы заправили автомобиль {self.model} бак заправлен на {self.fuel} ед. топлива можно ехать")
        else:
            print(f"Бак полон можно ехать")

    def __str__(self):
        return f"self.model = ({self.model}) \n" \
               f"self.fuel = ({self.fuel}) \n" \
               f"self.rate_fuel = ({self.rate_fuel}) \n"
